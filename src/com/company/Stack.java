package com.company;

import java.util.ArrayList;

public class Stack<T> {
    private int stackIdx;
    private ArrayList<T> arr;

    public Stack (){
        stackIdx = -1;
        arr = new ArrayList<>();
    }
    public void push(T data)
    {
        arr.add(data);
        stackIdx++;
        System.out.println('+');
    }
    public T pop()
    {
        T tmp  =arr.remove(stackIdx--);
        System.out.println('-');
        return  tmp;
    }

    public boolean SIsEmpty(){
        if(stackIdx == -1)
            return true;
        else
            return false;
    }
    public T showNextStack(){
        return arr.get(stackIdx);
    }
}
